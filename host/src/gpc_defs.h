/*
 * gpc_defs.h
 *
 * host library
 *
 *  Created on: April 23, 2021
 *      Author: A.Popov
 */

#ifndef CORE_DEFS_H_
#define CORE_DEFS_H_

//Macros for multicore
#define  			__foreach_group(group) \
						for (int (group)=0; (group)<LNH_GROUPS_COUNT; (group)++)
#define 			__foreach_core(group,core) \
						for (int (group)=0; (group)<LNH_GROUPS_COUNT; (group)++) \
						for (int (core)=LNH_CORES_LOW[(group)]; (core)<=LNH_CORES_HIGH[(group)]; (core)++)

#define 			LNH_MAX_CORES_IN_GROUP	6

//Cores soft specification
const struct {
	char* KERNEL_NAME[4][6] = {
			{		"gpn_singlecore_cacheu2a_hs:{lnh_g0_dp0_ip6_ir0}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g0_dp1_ip6_ir1}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g0_dp2_ip6_ir2}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g0_dp3_ip6_ir3}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g0_dp4_ip7_ir0}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g0_dp5_ip7_ir1}"
			},
			{		"gpn_singlecore_cacheu2a_hs:{lnh_g2_dp0_ip6_ir0}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g2_dp1_ip6_ir1}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g2_dp2_ip6_ir2}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g2_dp3_ip6_ir3}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g2_dp4_ip7_ir0}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g2_dp5_ip7_ir1}"
			},
			{		"gpn_singlecore_cacheu2a_hs:{lnh_g1_dp0_ip6_ir0}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g1_dp1_ip6_ir1}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g1_dp2_ip6_ir2}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g1_dp3_ip6_ir3}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g1_dp4_ip7_ir0}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g1_dp5_ip7_ir1}"
			},
			{		"gpn_singlecore_cacheu2a_hs:{lnh_g3_dp0_ip6_ir0}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g3_dp1_ip6_ir1}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g3_dp2_ip6_ir2}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g3_dp3_ip6_ir3}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g3_dp4_ip7_ir0}",
					"gpn_singlecore_cacheu2a_hs:{lnh_g3_dp5_ip7_ir1}"
			}
	};
	unsigned int LEONHARD_CONFIG[4][6] = {
			//For control logic = {index_region[1:0],index_partition[2:0],data_partition[2:0]} = {0,6,0} = 8'b00110000
			//Bank 0
			{
					((0<<6)|(6<<3)|(0<<0)), //Index_region = 0, Index_partition = 6, Data_partition = 0
					((1<<6)|(6<<3)|(1<<0)), //Index_region = 1, Index_partition = 6, Data_partition = 1
					((2<<6)|(6<<3)|(2<<0)), //Index_region = 2, Index_partition = 6, Data_partition = 2
					((3<<6)|(6<<3)|(3<<0)), //Index_region = 3, Index_partition = 6, Data_partition = 3
					((0<<6)|(7<<3)|(4<<0)), //Index_region = 0, Index_partition = 7, Data_partition = 4
					((1<<6)|(7<<3)|(5<<0))  //Index_region = 1, Index_partition = 7, Data_partition = 5
			},
			//Bank 1
			{
					((0<<6)|(6<<3)|(0<<0)), //Index_region = 0, Index_partition = 6, Data_partition = 0
					((1<<6)|(6<<3)|(1<<0)), //Index_region = 1, Index_partition = 6, Data_partition = 1
					((2<<6)|(6<<3)|(2<<0)), //Index_region = 2, Index_partition = 6, Data_partition = 2
					((3<<6)|(6<<3)|(3<<0)), //Index_region = 3, Index_partition = 6, Data_partition = 3
					((0<<6)|(7<<3)|(4<<0)), //Index_region = 0, Index_partition = 7, Data_partition = 4
					((1<<6)|(7<<3)|(5<<0))  //Index_region = 1, Index_partition = 7, Data_partition = 5
			},
			//Bank 2
			{
					((0<<6)|(6<<3)|(0<<0)), //Index_region = 0, Index_partition = 6, Data_partition = 0
					((1<<6)|(6<<3)|(1<<0)), //Index_region = 1, Index_partition = 6, Data_partition = 1
					((2<<6)|(6<<3)|(2<<0)), //Index_region = 2, Index_partition = 6, Data_partition = 2
					((3<<6)|(6<<3)|(3<<0)), //Index_region = 3, Index_partition = 6, Data_partition = 3
					((0<<6)|(7<<3)|(4<<0)), //Index_region = 0, Index_partition = 7, Data_partition = 4
					((1<<6)|(7<<3)|(5<<0))  //Index_region = 1, Index_partition = 7, Data_partition = 5
			},
			//Bank 3
			{
					((0<<6)|(6<<3)|(0<<0)), //Index_region = 0, Index_partition = 6, Data_partition = 0
					((1<<6)|(6<<3)|(1<<0)), //Index_region = 1, Index_partition = 6, Data_partition = 1
					((2<<6)|(6<<3)|(2<<0)), //Index_region = 2, Index_partition = 6, Data_partition = 2
					((3<<6)|(6<<3)|(3<<0)), //Index_region = 3, Index_partition = 6, Data_partition = 3
					((0<<6)|(7<<3)|(4<<0)), //Index_region = 0, Index_partition = 7, Data_partition = 4
					((1<<6)|(7<<3)|(5<<0))  //Index_region = 1, Index_partition = 7, Data_partition = 5
			}
	};




} LNH_CORE_DEFS;

//Define group count for Leonhard x64 version
#define 			LNH_GROUPS_COUNT 			2
const unsigned int 	LNH_CORES_LOW[4] 		= 	{	0,	0,	0xF,0xF	};
const unsigned int 	LNH_CORES_HIGH[4] 		= 	{	5,	5,	0,	0	};
const unsigned int	GPC_RESET_HIGH 			= 	1;
const unsigned int	GPC_RESET_LOW 			= 	0;
#define 			GLOBAL_MEMORY_MAX_LENGTH  	32768


#endif
