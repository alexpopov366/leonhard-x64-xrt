/*
 * gpc_io_host.cpp
 *
 * host library (XRT runtime version)
 *
 *  Created on: April 23, 2021
 *      Author: A.Popov
 */

#include "gpc_io_xrt_host.h"

// Read queue status from global memory

void gpc_io::update_host2gpc_queue_status()
{
	global_memory_pointer->sync(XCL_BO_SYNC_BO_FROM_DEVICE , sizeof(int)*2, host2gpc_head_offset);
	host2gpc_head = *(volatile unsigned int*)(global_memory_map + host2gpc_head_offset);
	host2gpc_tail = *(volatile unsigned int*)(global_memory_map + host2gpc_tail_offset);
}

void gpc_io::update_gpc2host_queue_status()
{
	global_memory_pointer->sync(XCL_BO_SYNC_BO_FROM_DEVICE , sizeof(int)*2, gpc2host_head_offset);
	gpc2host_head = *(volatile unsigned int*)(global_memory_map + gpc2host_head_offset);
	gpc2host_tail = *(volatile unsigned int*)(global_memory_map + gpc2host_tail_offset);
}

// Constructor
gpc_io	::	gpc_io(xrt::device *xrt_device,xrt::uuid *xrt_uuid,unsigned int group_number,unsigned int core_number)
		: 	gpc (xrt_device,xrt_uuid,group_number,core_number)
{
	// Init queues offsets
	gpc2host_queue = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + GPC2HOST_QUEUE;
	host2gpc_queue = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + HOST2GPC_QUEUE;
	gpc2host_head_offset = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + GPC2HOST_QUEUE + MSG_QUEUE_FSIZE - 8;
	gpc2host_tail_offset = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + GPC2HOST_QUEUE + MSG_QUEUE_FSIZE - 4;
	host2gpc_head_offset = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + HOST2GPC_QUEUE + MSG_QUEUE_FSIZE - 8;
	host2gpc_tail_offset = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + HOST2GPC_QUEUE + MSG_QUEUE_FSIZE - 4;
	// Init buffers offsets
	host2gpc_buffer = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + HOST2GPC_BUFFER;
	gpc2host_buffer = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + GPC2HOST_BUFFER;
}

// Constructor
gpc_io::~gpc_io() {
	free(mq_send_th);
	free(buf_write_th);
	free(mq_receive_th);
	free(buf_read_th);
}
//Thread version

// Load RV kernel binary to the GPN

void gpc_io::load_sw_kernel(char *gpnbin,xrt::bo *global_memory_ptr, xrt::bo *ddr_memory_ptr){
	// Map IO user space buffers to global memory
	gpc::load_sw_kernel(gpnbin, global_memory_ptr, ddr_memory_ptr);
	global_memory_map = global_memory_pointer->map<char*>();
}

// Send message to gpc
void gpc_io::mq_send_thread(unsigned int message)
{
	do	{ update_host2gpc_queue_status();
	} while ((host2gpc_tail + sizeof(unsigned int)) % MSG_QUEUE_SIZE == host2gpc_head); // Queue is full
	*(volatile unsigned int*)(global_memory_map + host2gpc_queue + host2gpc_tail) = message;
	global_memory_pointer->sync(XCL_BO_SYNC_BO_TO_DEVICE , sizeof(int), host2gpc_queue + host2gpc_tail);
	host2gpc_tail = (host2gpc_tail + sizeof(unsigned int)) % MSG_QUEUE_SIZE;
	*(volatile unsigned int*)(global_memory_map + host2gpc_tail_offset) = host2gpc_tail;
	global_memory_pointer->sync(XCL_BO_SYNC_BO_TO_DEVICE , sizeof(int), host2gpc_tail_offset);
}


// Send multiple messages to gpc
void gpc_io::mq_send_buf_thread(unsigned int size, unsigned int *buffer)
{
	for (unsigned int i=0; i<size; i+=4) {
		mq_send_thread(*(unsigned int*)((char*)buffer + i));
	}
}

// Wait and read single message from gpc
unsigned int gpc_io::mq_receive_thread()
{
	do	{ update_gpc2host_queue_status();
	} while (gpc2host_tail == gpc2host_head); // Queue is empty
	global_memory_pointer->sync(XCL_BO_SYNC_BO_FROM_DEVICE , sizeof(int), gpc2host_queue + gpc2host_head);
	unsigned int message = *(volatile unsigned int*)(global_memory_map + gpc2host_queue + gpc2host_head);
	gpc2host_head = (gpc2host_head + sizeof(unsigned int)) % MSG_QUEUE_SIZE;
	*(volatile unsigned int*)(global_memory_map + gpc2host_head_offset) = gpc2host_head;
	global_memory_pointer->sync(XCL_BO_SYNC_BO_TO_DEVICE , sizeof(int), gpc2host_head_offset);
	return message;
}

// Wait and read multiple messages from gpc
void gpc_io::mq_receive_buf_thread(unsigned int size, unsigned int *buffer)
{
	for (unsigned int i=0; i<size; i+=4) {
		*(unsigned int*)((char*)buffer + i) = mq_receive_thread();
	}
}

// Write data to HOST2GPC buffer
void gpc_io::buf_write_thread(unsigned int size, unsigned int *buffer) {
	for (unsigned int i = 0; i < size; i+=4)
		*(volatile unsigned int*)(global_memory_map + host2gpc_buffer + i) = *(unsigned int*)((char*)buffer + i);
	global_memory_pointer->sync(XCL_BO_SYNC_BO_TO_DEVICE, size, host2gpc_buffer);


}

// Read data from GPC2HOST buffer
void gpc_io::buf_read_thread(unsigned int size, unsigned int *buffer) {
	global_memory_pointer->sync(XCL_BO_SYNC_BO_FROM_DEVICE , size, gpc2host_buffer);
	for (unsigned int i = 0; i < size; i+=4)
		*(unsigned int*)((char*)buffer + i) = *(volatile unsigned int*)(global_memory_map + host2gpc_buffer + i);
}

// Send single message to gpc
void gpc_io::mq_send(unsigned int message)
{
	mq_send_th_alive.lock();
	mq_send_thread(message);
	mq_send_th_alive.unlock();
}

// Send multiple messages to gpc asynchronously
void gpc_io::mq_send(unsigned int size, unsigned int *buffer)
{
	mq_send_th_alive.lock();
    mq_send_th = new std::thread(&gpc_io::mq_send_buf_thread, this,  size, buffer);
	mq_send_th_alive.unlock();
}

// Wait for the
void gpc_io::mq_send_join()
{
	mq_send_th->join();
	free(mq_send_th);

}

// Wait and read single message from gpc
unsigned int gpc_io::mq_receive()
{
	mq_receive_th_alive.lock();
	unsigned int message  = mq_receive_thread();
	mq_receive_th_alive.unlock();
	return message;
}

void gpc_io::mq_receive(unsigned int size, unsigned int *buffer)
{
	mq_receive_th_alive.lock();
    mq_receive_th = new std::thread(&gpc_io::mq_receive_buf_thread, this,  size, buffer);
	mq_receive_th_alive.unlock();
}

void gpc_io::mq_receive_join()
{
	mq_receive_th->join();
	free(mq_receive_th);
}

// Write data to HOST2GPC buffer
void gpc_io::buf_write(unsigned int size, unsigned int *buffer)
{
	buf_write_th_alive.lock();
    buf_write_th = new std::thread(&gpc_io::buf_write_thread, this,  size, buffer);
	buf_write_th_alive.unlock();
}

void gpc_io::buf_write_join()
{
	buf_write_th->join();
	free(buf_write_th);
}

// Read data from GPC2HOST buffer
void gpc_io::buf_read(unsigned int size, unsigned int *buffer)
{
	buf_read_th_alive.lock();
    buf_read_th = new std::thread(&gpc_io::buf_read_thread, this,  size, buffer);
	buf_read_th_alive.unlock();
}

void gpc_io::buf_read_join()
{
	buf_read_th->join();
	free(buf_read_th);

}

// Syncronization point with gpc
void gpc_io::sync_with_gpc()
{
	mq_send(0);
	mq_receive();
}
